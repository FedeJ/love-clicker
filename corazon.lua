Corazon = Object:extend()

function Corazon.new(self, controlador)
  self.image = love.graphics.newImage("resourses/loveImage.png")
  self.width, self.height = self.image:getDimensions( )
  self.x = 270
  self.y = 380
  self.r = 0;
  self.sx = 0.3
  self.sy = 0.3
  self.controlador = controlador
  
  self.auxScaleX = self.sx
  self.auxScaley = self.sy
end

function Corazon.update(self, dt)
  if(love.mouse.isDown(1) == false and 
    (love.mouse.getX() >= self.x - self.width/2 * self.sx and 
     love.mouse.getY() >= self.y - self.height/2 * self.sy and 
     love.mouse.getX() <= self.x + self.width * self.sx - self.width/2 * self.sx and 
     love.mouse.getY() <= self.y + self.height * self.sy - self.height/2 * self.sy))
  then
    self.auxScaleX = self.sx-0.04
    self.auxScaley = self.sy-0.04
  else
    self.auxScaleX = self.sx
    self.auxScaley = self.sy
  end
end

function Corazon.draw(self)
  love.graphics.draw(self.image, self.x, self.y, self.r, self.auxScaleX, self.auxScaley, self.width/2, self.height/2)
end

function love.mousepressed(x, y, button, istouch)
   if (button == 1 and (love.mouse.getX() >= corazon.x - corazon.width/2 * corazon.sx and 
                        love.mouse.getY() >= corazon.y - corazon.height/2 * corazon.sy and 
                        love.mouse.getX() <= corazon.x + corazon.width* corazon.sx - corazon.width/2 * corazon.sx and 
                        love.mouse.getY() <= corazon.y + corazon.height* corazon.sy - corazon.height/2 * corazon.sy))
   then
    controlador:addLove()
    corazon.auxScaleX = corazon.sx
    corazon.auxScaley = corazon.sy
   end
end

