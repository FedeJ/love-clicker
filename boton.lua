Boton = Object:extend()

function Boton.new(self, x, y, text, font, price, poder)
  self.enable = false
  
  self.x = x
  self.y = y
  self.text = text 
  self.font = font
  self.width = 256
  self.height = 70

  self.cantidad = 0
  self.poder = poder
  self.price = price
  self.priceAux = price
  
  self.alphaMax = 0
  self.alphaCur = 0
  
  self.buttonText = love.graphics.newText(self.font, text)
  self.buttonText2 = love.graphics.newText(self.font, "Quantity:")
  self.buttonText3 = love.graphics.newText(self.font, self.cantidad)
  self.buttonText4 = love.graphics.newText(self.font, self.price)
  
  self.color1 = 0
  self.color2 = 0
  self.color3 = 0
  
  self.activado = false
  
end

function Boton.update(self, dt)
  if self.price/3 <= controlador.puntaje then self.enable = true end

  if self.enable == false then return end
  
  self.alphaCur = controlador.puntaje / self.price
  
  if self.alphaMax < self.alphaCur then self.alphaMax = self.alphaCur end
  if self.alphaMax > 1.05 then self.alphaMax = 1.05 end
  
  if(love.mouse.isDown(1) == true and 
    (love.mouse.getX() >= self.x and 
     love.mouse.getY() >= self.y and 
     love.mouse.getX() <= self.x + self.width and 
     love.mouse.getY() <= self.y + self.height))
  then
    if (self.activado == false) then
      if(self.cantidad < 100) then
        if(controlador.buyLove(controlador, self.price)) then
        self.cantidad = self.cantidad + 1
        self.price = math.floor(self.price + (self.cantidad * self.priceAux * 0.15))
        local auxCantidad = 0
        for i = 1,5 do
          auxCantidad = auxCantidad + botones[i].cantidad * botones[i].poder
        end
        controlador.textoLovePerSec = love.graphics.newText(self.font, auxCantidad)
        end
      end
      self.activado = true
      self.buttonText3 = love.graphics.newText(self.font, self.cantidad)
      self.buttonText4 = love.graphics.newText(self.font, self.price)
    end
    self.color1 = 64
    self.color2 = 224
    self.color3 = 208
  else
    self.activado = false
    self.color1 = 0
    self.color2 = 128
    self.color3 = 128
  end
end

function Boton.draw(self)
    if self.enable == false then return end
  
    local alphaAux = self.alphaMax * 256
  
    love.graphics.setColor(self.color1,self.color2,self.color3, alphaAux)
    love.graphics.rectangle("fill", self.x, self.y, self.width, self.height)
    
    love.graphics.setColor(self.color1 /3,self.color2 / 2,self.color3 / 2, alphaAux)
    love.graphics.rectangle("line", self.x, self.y, self.width, self.height)
    if self.alphaMax > 1 then
      love.graphics.draw(self.buttonText, self.x+10,self.y+10,0,0.40,0.40)
      love.graphics.draw(self.buttonText2, self.x+60,self.y+30,0,0.60,0.60)
      love.graphics.draw(self.buttonText3, self.x+185,self.y+31,0,0.60,0.60)
      love.graphics.draw(self.buttonText4, self.x+125,self.y+10,0,0.40,0.40)
    end
    
    love.graphics.setColor(256,256,256, 256)
end

function Boton.RefreshText(self)
    self.buttonText3 = love.graphics.newText(self.font, self.cantidad)
    self.buttonText4 = love.graphics.newText(self.font, self.price)
end