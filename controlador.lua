Controlador = Object:extend()

function Controlador.new(self, x, y, font)
  self.puntaje = 0
  self.x = x
  self.y = y 
  self.width = 500
  self.height = 70
  self.font = font
  
  self.color1 = 256
  self.color2 = 120
  self.color3 = 180
  
  self.time = 0
  
  self.textoPuntaje = love.graphics.newText(font, self.puntaje)
  self.textoLove = love.graphics.newText(font, "LOVE:")
  self.textoDiTextoLovePerSec = love.graphics.newText(font, "Love Per Sec:")
  self.textoLovePerSec = love.graphics.newText(font, "")
  
  self.textoReiniciar = love.graphics.newText(font, "Reiniciar")
  self.textoGuardar = love.graphics.newText(font, "Guardar")
  
  self.opciones = false
  self.guardado = false
end

function Controlador.update(self, dt)
  self.time = self.time + dt
  if(self.time >= 0.1) then
    self.time = 0
    for i = 1,5 do
      self.puntaje = self.puntaje + (botones[i].cantidad * botones[i].poder) * 0.1
    end
    self.textoPuntaje = love.graphics.newText(self.font, math.floor(self.puntaje))
  end
  
  if(love.mouse.isDown(1) == true and 
    (love.mouse.getX() >= love.graphics.getWidth() - 160 and 
    love.mouse.getY() >= 72 and 
    love.mouse.getX() <= love.graphics.getWidth() - 160 + 140 and 
    love.mouse.getY() <= 72 + 30))
  then
    if self.guardado==false then
      saveGame()
      self.guardado = true
    end
  else
    self.guardado=false
  end
  
  if(love.mouse.isDown(1) == true and 
    (love.mouse.getX() >= love.graphics.getWidth() - 160 and 
     love.mouse.getY() >= 12 and 
     love.mouse.getX() <= love.graphics.getWidth() - 160 + 140 and 
     love.mouse.getY() <= 12 + 30))
  then
      self.opciones=true
  end
  
  if self.opciones then
    if(love.mouse.isDown(1) == true and 
      (love.mouse.getX() >= love.graphics.getWidth() - 40 and 
       love.mouse.getY() >= 40 and 
       love.mouse.getX() <= love.graphics.getWidth() - 40 + 20 and 
       love.mouse.getY() <= 40 + 20))
    then
      self.opciones = false
    end
    if self.opciones then
      if(love.mouse.isDown(1) == true and 
        (love.mouse.getX() >= love.graphics.getWidth() - 70 and 
         love.mouse.getY() >= 40 and 
         love.mouse.getX() <= love.graphics.getWidth() - 70 + 20 and 
         love.mouse.getY() <= 40 + 20))
      then
        restartGame()
        self.opciones = false
      end
    end
  end
end

function Controlador.draw(self)
  love.graphics.setColor(self.color1,self.color2,self.color3)
  love.graphics.rectangle("fill", self.x, self.y, self.width, self.height)
  love.graphics.rectangle("fill", love.graphics.getWidth() - 160, 12, 140, 30)
  love.graphics.rectangle("fill", love.graphics.getWidth() - 160, 72, 140, 30)
  love.graphics.setColor(180,20,100)
  love.graphics.rectangle("line", self.x, self.y, self.width, self.height)
  love.graphics.rectangle("line", love.graphics.getWidth() - 160, 12, 140, 30)
  love.graphics.rectangle("line", love.graphics.getWidth() - 160, 72, 140, 30)
  love.graphics.draw(self.textoLove, self.x+10,self.y+10, 0, 0.85, 0.85)
  love.graphics.draw(self.textoPuntaje, self.x+150,self.y+10,0, 0.85, 0.85)
  
  love.graphics.draw(self.textoLovePerSec, 210, love.graphics.getHeight()- 40, 0, 0.6, 0.6)
  love.graphics.draw(self.textoDiTextoLovePerSec, 10, love.graphics.getHeight()- 40, 0, 0.6, 0.6)
  
  love.graphics.draw(self.textoReiniciar, love.graphics.getWidth() - 149, 10, 0,0.6,0.6)
  love.graphics.draw(self.textoGuardar, love.graphics.getWidth() - 146, 70, 0,0.6,0.6)
  
  if self.opciones then
    love.graphics.setColor(140,207,127)
    love.graphics.rectangle("fill", love.graphics.getWidth() - 70, 40, 20,20)
    love.graphics.setColor(227,135,108)
    love.graphics.rectangle("fill", love.graphics.getWidth() - 40, 40, 20,20)
  end
  
  love.graphics.setColor(256,256,256)
end

function Controlador.addLove(self)
  self.puntaje = self.puntaje + 1
  self.textoPuntaje = love.graphics.newText(self.font, self.puntaje)
end

function Controlador.buyLove(self, price)
  if (self.puntaje >= price) then
    self.puntaje = self.puntaje - price
    return true
  else
    return false
  end
end

function Controlador.RefreshText(self)
    self.buttonText3 = love.graphics.newText(self.font, self.cantidad)
    self.buttonText4 = love.graphics.newText(self.font, self.price)
    local auxCantidad = 0
    for i = 1,5 do
      auxCantidad = auxCantidad + botones[i].cantidad * botones[i].poder
    end
    controlador.textoLovePerSec = love.graphics.newText(self.font, auxCantidad)
end