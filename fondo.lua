Fondo = Object:extend()

function Fondo.new(self)
  self.image = love.graphics.newImage("resourses/paritulas.png")
  self.xo, self.yo = self.image:getDimensions( )
  
  self.x = 270
  self.y = 380
  self.r = 0;
  self.sx = 1
  self.sy = 1
end

function Fondo.update(self, dt)
  self.r = self.r + 0.5 * dt;
end

function Fondo.draw(self)
  love.graphics.draw(self.image, self.x, self.y, self.r, self.sx, self.sy, self.xo/2, self.yo/2)
end