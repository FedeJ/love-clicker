function love.load()
  Object = require "classic"
  x = 100 
  require "corazon"
  require "boton"
  require "fondo"
  require "controlador"
  require "SaveTableToFile"
  
  cursor = love.mouse.newCursor("resourses/loveCursor.png",0,0)
  love.mouse.setCursor(cursor)
  textFont = love.graphics.newFont("resourses/arial.ttf", 50)
  love.graphics.setBackgroundColor(217,179,255)
  
  controlador = Controlador(10, 10, textFont)
  corazon = Corazon(controlador)
  fondo = Fondo()

  textoBotones = {"Love","GreatLove","GreaterLove","SuperLove","LOVE"}
  precioBotones = { 10, 100, 1100, 12000, 150000 }
  
  botones = {}
  
  createButtons(5, 70)

  loadGame()
  

  
end

function love.update(dt)
  corazon:update(dt)
  fondo:update(dt)
  controlador:update(dt)
  
  for i = 1,5 do
    botones[i]:update(dt)
  end
end

function love.draw()
  fondo:draw()
  corazon:draw()
  controlador:draw()
  
  for i = 1,5 do
    botones[i]:draw()
  end
end


function createButtons()
  for i = 1,5 do
    boton = Boton(love.graphics.getWidth() - 270, love.graphics.getHeight()*(1/3) + (70+8) * i - 70, textoBotones[i], textFont, precioBotones[i], 0.1 * math.pow(5,i))
    table.insert(botones, boton)
  end
end

function restartGame()
  controlador.puntaje=0
  controlador.time = 0
  controlador.textoLovePerSec = love.graphics.newText(textFont, "")
  for i=1,#botones do
    botones[i].enable = false
    botones[i].alphaMax = 0
    botones[i].alphaCur = 0
    botones[i].activado = false
    botones[i].cantidad = 0
    botones[i].price = botones[i].priceAux
    botones[i].RefreshText(botones[i])
  end
end

function saveGame()
  
  local gameData = {}
  
  table.insert(gameData, controlador.puntaje)
  for i=1, #botones do
    table.insert(gameData, botones[i].cantidad)
    table.insert(gameData, botones[i].alphaMax)
    table.insert(gameData, botones[i].enable)
    table.insert(gameData, botones[i].price)
  end
  
  table.save(gameData, "saveData")
end

function loadGame()
 
    local gameData = table.load("saveData")
    
    if gameData == nil then return end
    controlador.puntaje = gameData[1]
    botones[1].cantidad = gameData[2]
    botones[1].alphaMax = gameData[3]
    botones[1].enable = gameData[4]
    botones[1].price = gameData[5]
    botones[2].cantidad = gameData[6]
    botones[2].alphaMax = gameData[7]
    botones[2].enable = gameData[8]
    botones[2].price = gameData[9]
    botones[3].cantidad = gameData[10]
    botones[3].alphaMax = gameData[11]
    botones[3].enable = gameData[12]
    botones[3].price = gameData[13]
    botones[4].cantidad = gameData[14]
    botones[4].alphaMax = gameData[15]
    botones[4].enable = gameData[16]
    botones[4].price = gameData[17]
    botones[5].cantidad = gameData[18]
    botones[5].alphaMax = gameData[19]
    botones[5].enable = gameData[20]
    botones[5].price = gameData[21]
    
    for i=1, #botones do
      botones[i].RefreshText(botones[i])
    end
    controlador.RefreshText(controlador)
end